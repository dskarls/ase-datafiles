from pathlib import Path
import re
from setuptools import setup, find_packages

def all_datafiles():
    datafile_dir = Path(__file__).parent / 'asetest/datafiles'
    all_files = []
    for path in datafile_dir.rglob('*'):
        filename = str(path.relative_to(datafile_dir))
        all_files.append(filename)
    return all_files


package_data = {'asetest.datafiles': all_datafiles()}

def get_version_number():
    with open('asetest/__init__.py') as fd:
        return re.search(r"__version__ = '(.+)'", fd.read()).group(1)

setup(
    name='ase-testing',
    version=get_version_number(),
    packages=find_packages(),
    url='https://gitlab.com/ase/ase-testing',
    maintainer='ASE community',
    maintainer_email='ase-users@listserv.fysik.dtu.dk',
    package_data=package_data,
    description='Datafiles for systematic testing of ASE calculators'
)
